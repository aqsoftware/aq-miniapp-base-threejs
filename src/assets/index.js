//@flow
import crate from './textures/crate_color8.jpg';

/* Define common assets here */
const Assets = {
  images: {
  },
  textures: {
    crate: crate
  },
  sounds: {
  },
  fonts:{
  }
}

/* Array of common assets to be used by Hexi Loader */
export const ASSETS = [
  Assets.textures.crate
];

export default Assets;
