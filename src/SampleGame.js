// @flow
import {
  LifeCycle
} from 'aq-miniapp-core';
import Game from './components/Game';
import Assets, { ASSETS } from './assets';

const THREE = window.THREE;

type Props = {
  additionalInfo: {
    background: string
  }
}

export default class SampleGame extends Game<Props> {

  geometry: THREE.Geometry;
  texture: THREE.Texture;
  material: THREE.Material;
  mesh: THREE.Mesh;
  ambientLight: THREE.Light;
  directionalLight: THREE.Light;
  camera: THREE.Camera;

  gameDidMount() {
    // Do loading of assets here, passing our THREE.LoadingManager
    // instance to each loader needed. After loading, assetsDidLoad()
    // will be called
    this.texture = new THREE.TextureLoader(this.loadingManager).load(Assets.textures.crate);    
  }

  assetsDidLoad() {
    const size = this.renderer.getSize();
    this.camera = new THREE.PerspectiveCamera(70, size.width / size.height, 0.01, 10);
    this.camera.position.z = 1;

    this.geometry = new THREE.BoxGeometry(0.3, 0.3, 0.3);
    this.material = new THREE.MeshPhongMaterial({ color: 0xffffff, map: this.texture});
    this.mesh = new THREE.Mesh(this.geometry, this.material);

    this.scene.add(this.mesh);

    this.ambientLight = new THREE.AmbientLight(0xffffff, 0.4);
    this.scene.add(this.ambientLight);

    this.directionalLight = new THREE.DirectionalLight(0xffffff, 1);
    this.directionalLight.position.set(1, 1, 1);
    this.scene.add(this.directionalLight);

    this.animate();

    LifeCycle.informReady();
  }

  gameDidReset(newProps: Props) {
    // Handle game reset here with new data    
  }

  gameWillUnmount() {
    // Do cleanup here
    this.scene.remove(this.mesh);
    this.geometry.dispose();
    this.material.dispose();
    this.texture.dispose();
    this.ambientLight.dispose();
    this.directionalLight.dispose();
  }

  render() {
    this.mesh.rotation.x += 0.01;
    this.mesh.rotation.y += 0.01;

    this.renderer.render(this.scene, this.camera);
  }
}